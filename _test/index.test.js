const main = require("../index");

test("return hello world", () => {
  expect(main()).toBe("Hello World");
});
